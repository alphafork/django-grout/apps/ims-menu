from ims_base.apis import BaseAPIView
from ims_base.serializers import BaseModelSerializer
from rest_framework.viewsets import ReadOnlyModelViewSet

from ims_menu.models import Menu


class UserMenuAPI(BaseAPIView, ReadOnlyModelViewSet):
    def get_serializer_class(self):
        serializer_class = BaseModelSerializer
        serializer_class.Meta.model = Menu
        return serializer_class

    def get_queryset(self):
        return Menu.objects.filter(
            user_group__in=self.request.user.groups.all()
        ).filter(is_active=True)
