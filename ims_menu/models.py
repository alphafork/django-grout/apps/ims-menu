from django.contrib.auth.models import Group
from django.db import models
from ims_base.models import AbstractBaseDetail


class Menu(AbstractBaseDetail):
    class Meta:
        ordering = ["order"]

    name = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    slug = models.CharField(max_length=255, null=True, blank=True)
    icon = models.TextField(null=True, blank=True)
    user_group = models.ManyToManyField(Group, blank=True)
    parent = models.ForeignKey("self", on_delete=models.CASCADE, null=True, blank=True)
    is_active = models.BooleanField(default=True)
    order = models.PositiveSmallIntegerField(null=True, blank=True)

    def __str__(self):
        return self.slug
