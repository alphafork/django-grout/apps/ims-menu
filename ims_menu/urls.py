from django.urls import include, path
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from .apis import UserMenuAPI

router = routers.SimpleRouter()
router.register(r"", UserMenuAPI, "user-menu")


urlpatterns = [
    path("usermenu/", include(router.urls)),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=["json"])
