from django.apps import AppConfig


class IMSMenuConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_menu"

    model_strings = {
        "MENU": "Menu",
        "PARENT": "Menu",
    }
